import express from 'express';
import auth from '../middleware/jwt';
const routes = express.Router();
import * as authCotroller from '../controllers/user.controller';
import upload from '../middleware/multer';

routes.get('/', auth, authCotroller.getUsers);
routes.post('/', upload.single('image'), authCotroller.register);
routes.post('/login', authCotroller.login);
routes.put('/:id', auth, upload.single('image'), authCotroller.update);
routes.delete('/:id', auth, authCotroller.deleteUser);

export default routes;
