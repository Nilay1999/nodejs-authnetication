import express from 'express';
import auth from '../middleware/jwt';
const routes = express.Router();
import { productController } from '../controllers';
import upload from '../middleware/multer';

routes.get('/', auth, productController.getProduct);
routes.post('/', auth, upload.single('image'), productController.addProduct);
routes.post('/addToCart/:id', auth, productController.addToCart);
routes.put('/checkout/:id', auth, productController.checkout);
routes.put(
    '/:id',
    auth,
    upload.single('image'),
    productController.updateProduct
);
routes.delete('/:id', auth, productController.deleteProduct);

export default routes;
