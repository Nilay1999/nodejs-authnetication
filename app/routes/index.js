import express from 'express';
import * as UserRoute from './user.route';
import * as ProductRoute from './product.route';

const routes = express.Router();

routes.use('/user', UserRoute.default);
routes.use('/product', ProductRoute.default);
export default routes;
