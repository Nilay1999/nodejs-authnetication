import sendmail from '../helper/nodeMailer';
import Cart from '../schema/cart.schema';
import Order from '../schema/order.schema';
import Product from '../schema/product.schema';
import User from '../schema/user.schema';

export const getProduct = async (req, res) => {
    const { id } = req.params;
    let product;
    if (!id) {
        product = await Product.find({});
        res.json({ product });
    } else {
        product = await Product.findById(id);
        res.json({ product });
    }
};

export const addProduct = async (req, res) => {
    const { price, name } = req.body;
    const image = req.file.path;
    const userData = req.data.user;
    const { role, _id } = userData;
    if (role == 1 || role == 3) {
        if (!price || !name) {
            res.json({
                message: 'Please enter all info',
            });
        } else {
            const product = await Product.create({
                price,
                name,
                image,
                addedBy: _id,
            });

            res.json({
                message: 'Product added',
                data: product,
            });
        }
    } else {
        res.json({
            message: 'Not allowed',
        });
    }
};

export const updateProduct = async (req, res) => {
    let product;
    const userData = req.data.user;
    const { role, _id } = userData;
    if (role == 1 || role == 3) {
        if (!req.file) {
            product = await Product.findByIdAndUpdate(
                req.params.id,
                { addedBy: _id, ...req.body },
                { new: 1 }
            );
        } else {
            product = await Product.findByIdAndUpdate(
                req.params.id,
                { addedBy: _id, ...req.body, image: req.file.path },
                { new: 1 }
            );
        }
        res.json({
            message: 'User data updated',
            data: product,
        });
    } else {
        res.json({
            message: 'Not allowed',
        });
    }
};

export const deleteProduct = async (req, res) => {
    await Product.findByIdAndUpdate(req.params.id);
    res.json({ message: ' Product deleted ' });
};

export const addToCart = async (req, res) => {
    const productid = req.params.id;
    const userData = req.data.user;
    const { role, _id } = userData;
    let cart,
        total = 0;
    const isCartExists = await Cart.findOne({ userId: _id });
    if (!isCartExists) {
        const productAvailable = await Product.findOne({
            id: productid,
        }).select('totalQuantity');
        if (productAvailable.totalQuantity > 0) {
            let cartData = await Cart.create({
                userId: _id,
            });
            cart = await Cart.findOneAndUpdate(
                { id: cartData.id },
                {
                    $push: {
                        items: productid,
                    },
                },
                { new: true }
            ).populate('items');

            cart.items.forEach((ele) => {
                total = ele.price;
            });

            await Cart.findOneAndUpdate(
                {
                    id: cartData.id,
                },
                { total: total },
                { new: true }
            );

            await Product.findByIdAndUpdate(productid, {
                $inc: {
                    totalQuantity: -1,
                },
            });
        } else {
            res.json({ message: 'Out of stock' });
        }
    } else {
        cart = await Cart.findOneAndUpdate(
            { userId: _id },
            {
                $push: {
                    items: productid,
                },
            },
            { new: true }
        ).populate('items');

        cart.items.forEach(async (ele) => {
            total += ele.price;
        });

        await Product.findByIdAndUpdate(productid, {
            $inc: {
                totalQuantity: -1,
            },
        });

        await Cart.findOneAndUpdate(
            {
                id: cart.id,
            },
            { total: total },
            { new: true }
        );
    }

    res.json({ message: 'Product Added to cart', data: cart });
};

export const checkout = async (req, res) => {
    const { id } = req.params;
    const userData = req.data.user;
    const { _id } = userData;
    const cartItem = await Cart.findById(id)
        .populate('items')
        .select({ items: 1, total: 1 });
    const userEmail = await User.findById(_id).select('email');
    const newOrder = await Order.create({
        userId: _id,
        total: cartItem.total,
    });

    cartItem.items.forEach(async (ele) => {
        await Order.findByIdAndUpdate(newOrder.id, {
            $push: { items: ele.id },
        });
    });

    await Cart.findByIdAndUpdate(id, { $set: { items: [], total: 0 } });
    // await sendmail(
    //     userEmail.email,
    //     'Purchase Successfull',
    //     `Your order has succesfully completed . Your order ID is ${newOrder.id}`
    // );
    res.json(newOrder);
};
