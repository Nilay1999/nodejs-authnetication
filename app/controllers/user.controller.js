import User from '../schema/user.schema';
import jwt from 'jsonwebtoken';
import sendmail from '../helper/nodeMailer';

export const login = async (req, res) => {
    const { email, password } = req.body;

    if (!email || !password) {
        res.json({ message: 'Enter email or password ' });
    } else {
        const user = await User.findOne({ email: email });
        if (!user) {
            res.json({ message: 'User not found' });
        } else {
            const token = jwt.sign({ user }, process.env.JWT_SECRET);
            res.json({
                token,
                user: {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                },
            });
        }
    }
};

export const getUsers = async (req, res, next) => {
    const userData = req.data.user;
    const { role, _id } = userData;

    if (role == 1) {
        const users = await User.find({});
        res.json(users);
    } else {
        const user = await User.findOne({ id: _id });
        res.json(user);
    }
};

export const register = async (req, res) => {
    const { username, password, email, age, userRole } = req.body;
    const imageFile = req.file.path;
    if (!username || !password || !email || !age) {
        res.json({
            message: 'Please enter all fields',
        });
    } else {
        const isUserExist = await User.findOne({
            username: username,
            email: email,
        });

        if (!isUserExist) {
            const user = new User({
                username,
                password,
                email,
                age,
                profileImage: imageFile,
                role: userRole,
            });

            let userData = await user.save();
            // await sendmail(
            //     email,
            //     'Registration Successfull',
            //     `Your registration has been completed successfully`
            // );
            res.json({
                message: 'User added',
                data: userData,
            });
        } else {
            res.json({
                message: 'Same username has been already taken',
            });
        }
    }
};

export const update = async (req, res) => {
    const userData = req.data.user;
    const { _id, role } = userData;

    if (role == 1) {
        let data;
        if (!req.file) {
            data = await User.findOneAndUpdate(
                { id: req.params.id },
                {
                    ...req.body,
                },
                {
                    new: 1,
                }
            );
        } else {
            const file = req.file.path;
            data = await User.findOneAndUpdate(
                { id: req.params.id },
                {
                    ...req.body,
                    profileImage: file,
                },
                {
                    new: 1,
                }
            );
        }
        res.json({
            message: 'User data updated',
            data: data,
        });
    } else {
        res.json({
            message: 'Not allowed',
        });
    }
};

export const deleteUser = async (req, res) => {
    const { id } = req.params;
    await User.findByIdAndDelete(id);
    res.json({
        message: 'User deteled ',
    });
};
