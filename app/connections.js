import mongoose from 'mongoose';
import ENV from './env';
const { dbUrl } = ENV;
mongoose.connect(dbUrl).catch((err) => {
    console.log(err);
});

mongoose.connection.once('open', () => {
    console.log('MongoDB database connection established successfully');
});

export default mongoose;
