import express from 'express';
import cors from 'cors';
import http from 'http';
import * as Routes from './routes';
import './connections';
import ENV from './env';
import bodyparser from 'body-parser';

const app = express();
const server = http.createServer(app);

const { port } = ENV;

app.use('/app/uploads', express.static('app/uploads'));
app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
    res.send({ working: 'Working ....' });
});
app.use('/api', Routes.default);

server.listen(port, () => {
    console.log(`Server is up and running at http://localhost:${port}`);
});
