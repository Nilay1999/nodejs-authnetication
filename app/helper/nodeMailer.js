import nodemailer from 'nodemailer';

export const sendmail = async (email, subject, text) => {
    let mail = nodemailer.createTransport({
        host: 'smtp.mailtrap.io',
        port: 2525,
        secure: false,
        auth: {
            user: 'b3c2749f2ed7cc',
            pass: 'aca1f4f03679d8',
        },
    });

    let info = await mail.sendMail({
        from: 'nodedemo@yupmail.com',
        to: email,
        subject: subject,
        text: text,
    });

    return info;
};

export default sendmail;
