import jwt from 'jsonwebtoken';

const auth = (req, res, next) => {
    try {
        const token = req.header('Authorization');
        if (!token) {
            res.status(401).json({
                msg: 'No authentication token, authorization denied !.',
            });
        }

        const user = jwt.verify(token, process.env.JWT_SECRET);
        req.data = user;

        if (!user) {
            res.status(401).json({
                msg: 'Token verification failed, authorization denied !.',
            });
        }

        next();
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

export default auth;
