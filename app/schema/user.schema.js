import * as mongoose from 'mongoose';

const userScheam = new mongoose.Schema(
    {
        username: {
            type: mongoose.Schema.Types.String,
            required: true,
        },
        password: {
            type: mongoose.Schema.Types.String,
            required: true,
        },
        email: {
            type: mongoose.Schema.Types.String,
            required: true,
        },
        age: {
            type: mongoose.Schema.Types.Number,
            required: true,
        },
        profileImage: {
            type: mongoose.Schema.Types.String,
        },
        role: {
            type: mongoose.Schema.Types.Number,
            required: true,
            default: 2,
        },
    },
    { timestamps: true }
);

const User = mongoose.model('User', userScheam);

export default User;
