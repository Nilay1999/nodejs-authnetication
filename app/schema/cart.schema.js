import * as mongoose from 'mongoose';
const cartScheama = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        items: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Product',
            },
        ],
        total: {
            type: mongoose.Schema.Types.Number,
        },
    },
    { timestamps: true }
);

const Cart = mongoose.model('Cart', cartScheama);

export default Cart;
