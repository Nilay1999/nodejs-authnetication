import * as mongoose from 'mongoose';

const productScheama = new mongoose.Schema(
    {
        name: {
            type: mongoose.Schema.Types.String,
            required: true,
        },
        price: {
            type: mongoose.Schema.Types.Number,
            required: true,
        },
        image: {
            type: mongoose.Schema.Types.String,
        },
        addedBy: {
            type: mongoose.Schema.Types.String,
            ref: 'User',
        },
        totalQuantity: {
            type: mongoose.Schema.Types.Number,
            default: 0,
            min: 0,
        },
    },
    { timestamps: true }
);

const Product = mongoose.model('Product', productScheama);

export default Product;
