import dotenv from 'dotenv';

dotenv.config();

export default {
    secret: process.env.JWT_SECRET,
    port: process.env.PORT || 3000,
    dbUrl: process.env.DBURL,
};
